<?php

/**
 * @file
 * Template for packing slip pdf
 *
 * checkout packing-slip comments
 */

?>

<head>
  <?php foreach($css_files as $css_file): ?>
    <link rel="stylesheet" type="text/css" href="<?php print $css_file; ?>">
  <?php endforeach; ?>
</head>

<body>
<div class="packing-slip">
<?php print $order; ?>

</div>
</body>

