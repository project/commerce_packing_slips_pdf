<?php

/**
 * @file
 * File contain callback for pages
 */

/**
 * Page callback to display download link for zipped pdf then redirect back to
 * orders page.
 */
function commerce_packing_slip_pdf_download() {
  $page = array();
  $page['download'] = array(
    '#type' => 'markup',
    '#markup' => t('The download will begin automatically. Click !here to begin download if it does not.', array(
      '!here' => l(t('here'), '/sites/default/files/commerce_packing_slips_pdf.zip')
    )),
  );

  $zipfile = 'sites/default/files/commerce_packing_slips_pdf.zip';

  $rootPath = realpath('sites/default/files/commerce_packing_slips_pdf/');
  // Initialize archive object
  $zip = new ZipArchive();
  $zip->open($zipfile, ZipArchive::CREATE | ZipArchive::OVERWRITE);

  // Create recursive directory iterator
  /** @var SplFileInfo[] $files */
  $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);

  foreach ($files as $name => $file) {
    // Skip directories (they would be added automatically)
    if (!$file->isDir()) {
      // Get real and relative path for current file
      $filePath = $file->getRealPath();
      $relativePath = substr($filePath, strlen($rootPath) + 1);

      // Add current file to archive
      $zip->addFile($filePath, $relativePath);
    }
  }
  // Zip archive will be created only after closing object
  $zip->close();

  header("refresh:3;url=/$zipfile");
  // Remove the directory now that it has been zipped and served to download
  // at this point.
  recursiveRmDir($rootPath);
  return $page;
}

/**
 * Helper function to empty to the directory
 *
 * @see https://philio.me/recursively-remove-a-directory-in-php-using-spl/
 * @param $dir
 */
function recursiveRmDir($dir) {
  $iterator = new RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::CHILD_FIRST);
  foreach ($iterator as $filename => $fileInfo) {
    if ($fileInfo->isDir()) {
      rmdir($filename);
    } else {
      unlink($filename);
    }
  }
}