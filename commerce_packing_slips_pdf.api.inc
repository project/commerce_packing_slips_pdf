<?php

/**
 * @file
 * Hooks provided by Commerce packing slips pdf
 */

/**
 * This will allow you to change the view that will be called to be converted to
 * pdf as well as add your css files.
 *
 * Usually, if a module wanted to alter the VBO form through hook_form_alter(),
 * it would need to duplicate the views form checks from
 * views_bulk_operations_form_alter(), while making sure that the hook
 * runs after VBO's hook (by increasing the weight of the altering module's
 * system entry). In order to reduce that complexity, VBO provides this hook.
 *
 * @param $order
 *  The order entity.
 *    - Use this to pass the order id to the contextual filter of your view
 * @param $vars
 *  The data that will be sent to be rendered
 */
function hook_commerce_packing_slip_pdf_html_alter($order, &$vars) {
  $vars['order'] = views_embed_view('my_custom_view', 'default', $order->order_id);
}
